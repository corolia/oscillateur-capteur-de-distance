L'application à été fait sur localhost comme elle n'était pas faite pour aller en ligne. 

Elle nécessite les librairie createjs et Tone.js pour fonctionner.

Évidement, le capteur de distance est une composante physique que j'ai assemblé moi-même avec des fil et un Arduino, donc il faudra vous procurer les pièces nécessaires si vous voulez répliquer l'application.