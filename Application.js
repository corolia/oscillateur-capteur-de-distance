//Les constantes ci-dessous sont nécessaire au fonctionnement de l'application. 
// -- Firmata, Serialport et Johnny-five sont tous relier à créer ou améliorer la communication et la compatibilité entre le arduino et l'ordinateur.
// -- AMSynth est un objet provenant de la librairie Tone.js, c'est lui qui produira notre sons de départ.

const five = require("johnny-five");
const SerialPort = require("chrome-apps-serialport").SerialPort;
const Firmata = require("firmata-io")(SerialPort);
const {AMSynth} = require("tone");

export class Application {

  constructor() {
    this.canvas = null;
    this.chargeur = null;
    this.stage = null;
    this.delcolor = null;


    //Ici nous avons nos incréments de distance en cm pour chaques note qui sera jouer.
    this.distance = {min: 5, do: 10, re: 15, mi: 20, fa: 25, sol: 30, la: 35, si: 40, max: 45};

    //Puis une simple formule qui nous donne la zone d'intération, car on ne peut être trop proche, ni trop loin du capteur pour que ça fontionne.
    this.distance.delta = this.distance.max - this.distance.min;
 
    this.volume = {min: -12, max: -6}; //Volume max et minimum. Assez simple à comprendre!

    nw.Window.get().showDevTools(); // Surtout pour mieux débogger.

    //Pendant que l'application est chargée, s'il y a une erreur, notre fonction interrompre sera appelée.
    this
      .precharger("ressources/manifest.json")
      .then(this.initialiserCreateJS.bind(this))
      .then(this.initialiserJ5.bind(this))
      .then(this.demarrer.bind(this))
      .catch(this.interrompre.bind(this));
  }

  //Affiche un petit message d'erreur dans la fenêtre.
  interrompre(erreur) {
    console.error(erreur);
    alert(erreur);
  }

  //C'est ici que l'application est chargé comme mentionné plus haut.
  precharger(manifeste) {
    console.log("Preloading manifest");
    return new Promise((resolve, reject) => {
      this.chargeur = new createjs.LoadQueue();
      this.chargeur.installPlugin(createjs.Sound);
      this.chargeur.addEventListener("complete", resolve);
      this.chargeur.addEventListener("error", reject);
      this.chargeur.loadManifest(manifeste);
    });
  }

  //Cette fonction précise les réglages de la scène où notre application fonctionnera.
  initialiserCreateJS() {
    console.log("create.js is on");
    this.canvas = document.querySelector("canvas");
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.stage = new createjs.StageGL(this.canvas, {antialias: true});
    this.stage.setClearColor("#000");
    createjs.Ticker.on("tick", e => this.stage.update(e));
    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.framerate = 60;
  }

  //Ça c'est pour vérifier que le arduino est bien branché. Si le message d'erreur est afficher, je conseille de vérifier le branchement et les ports.Le nombre de fois où j'ai oublié de le brancher est incomptable. :(
  initialiserJ5() {
    console.log(`Looking for an Arduino`);
    return new Promise((resolve, reject) => {
      const carte = new five.Board({
        io: new Firmata("COM3")
      });
      carte.on("ready", resolve);
      carte.on("fail", reject);
    });
  }

  //Maintenant, l'application fonctionne. (Kinda)
  demarrer() {
    console.log("Starting...");
    this.affichage = new createjs.Text("SOL", "80px Impact", "#FFFFFF"); //La meilleur font.
    this.affichage.cache(0, 0, this.affichage.getMeasuredWidth(), this.affichage.getMeasuredHeight() + 5);
    this.affichage.x = this.canvas.width / 2 - this.affichage.getMeasuredWidth() / 2;
    this.affichage.y = 250;
    this.stage.addChild(this.affichage);

    //Voici la partie plus musique du code, c'est ici les réglages de notre objet Tonejs. Je suis loin d'être musicien donc je vous laisse jouer avec vous-même.
    this.amsynth = new AMSynth({
      harmonicity : 3 ,
      detune : 0 ,
      oscillator : {
        type : "sine"
      } ,
      envelope : {
        attack : 0.01 ,
        decay : 0.01 ,
        sustain : 1 ,
        release : 0.5
      } ,
      modulation : {
        type : "square"
      } ,
      modulationEnvelope : {
        attack : 0.5 ,
        decay : 0 ,
        sustain : 1 ,
        release : 0.5
      }
    }).toMaster();
    this.amsynth.volume.value = this.volume.min;

    //Ici j'avais ajouter une DEL qui affiche une couleur différente pour chaque note simplement pour les point bonis, mais ce n'est pas du tout nécessaire. Si vous voulez de la lumière, juste vous assurez que chaque broche est au bon endroit selon les numéros spécifiés plus bas.
    this.delcolor = new five.Led.RGB({
      pins: {
        red: 6,
        green: 5,
        blue: 3
      }
    });
    this.delcolor.intensity(10);
    this.delcolor.blink(500);// Vitesse en miliseconde du clignotement

    //Ici nous avons des réglages qui sont passés à QuickSettings pour pouvoir les modifier en temps réel et tester, plutôt que de revenir dans le code chaque fois.
    this.param = QuickSettings.create(0, 0, "Paramètres")
      .addRange("Harmonicity", 0.5, 4.0, 3.0, 0.1)
      .addDropDown("Oscillation", ["sine", "square", "triangle", "sawtooth"], parametre => {
        this.amsynth.oscillator.type = parametre.value;
      })
      .addRange("Attack", 0.01, 2.00, 0.10, 0.01)
      .addRange("Decay", 0.01, 2.00, 0.20, 0.01)
      .addRange("Sustain", 0.00, 1.00, 0.20, 0.01)
      .addRange("Release", 0.01, 4.00, 0.30, 0.01)
      .addDropDown("Modulation", ["sine", "square", "triangle", "sawtooth"], parametre => {
        this.amsynth.modulation.type = parametre.value;
      })
      .addRange("Modulator Attack", 0.01, 2.00, 0.50, 0.01)
      .addRange("Modulator Decay", 0.01, 2.00, 0.01, 0.01)
      .addRange("Modulator Sustain", 0.00, 1.00, 1.00, 0.01)
      .addRange("Modulator Release", 0.01, 4.00, 0.50, 0.01);

    this.capteur = new five.Proximity({
      controller: "2Y0A21",
      pin: "A0",
      freq: 500
    });

    this.capteur.on("data", this.ajuster.bind(this));//Cette ligne sert à mettre à jour notre affichage selon l'intéraction pendant que notre capteur est activé.
  }

  ajuster(e) {
    //Si l'utilisateur est entre nos deux limites.
    if (e.cm > this.distance.min && e.cm < this.distance.max) {

      if (this.amsynth.volume.value < this.volume.max) {
        this.amsynth.volume.cancelAndHoldAtTime();
        this.amsynth.volume.rampTo(this.volume.max, 1);
      }

      //Dernière chose ajustable: les notes! Tone est en anglais donc il faut faire un peu d'ajustement, mais une fois terminé, ont ne fait qu'entrer la note voulu et sa mesure. Une couleur aussi pour les différencier. Le code ci-dessous ne couvre qu'une seule gamme et non un piano entier donc il est possible d'en ajouter beaucoup.
      if(e.cm > this.distance.min && e.cm < this.distance.do){
        this.amsynth.triggerAttackRelease('A4', '8n');
        this.affichage.text = "DO";
        this.delcolor.color("#FF00FF");
        this.affichage.color = "#FF00FF";
      }else if(e.cm > this.distance.do && e.cm < this.distance.re){
         this.amsynth.triggerAttackRelease('B4', '8n');
         this.affichage.text = "RE";
         this.delcolor.color("#DC0073");
         this.affichage.color = "#DC0073";
      }else if(e.cm > this.distance.re && e.cm < this.distance.mi){
         this.amsynth.triggerAttackRelease('C4', '8n');
         this.affichage.text = "MI";
         this.delcolor.color("#FF0000");
         this.affichage.color = "#FF0000";
      }else if(e.cm > this.distance.mi && e.cm < this.distance.fa){
         this.amsynth.triggerAttackRelease('D4', '8n');
         this.affichage.text = "FA";
         this.delcolor.color("#C200FB");
         this.affichage.color = "#C200FB";
      }else if(e.cm > this.distance.fa && e.cm < this.distance.sol){
         this.amsynth.triggerAttackRelease('E4', '8n');
         this.affichage.text = "SOL";
         this.delcolor.color("#0000FF");
         this.affichage.color = "#0000FF";
      }else if(e.cm > this.distance.sol && e.cm < this.distance.la){
         this.amsynth.triggerAttackRelease('F4', '8n');
         this.affichage.text = "LA";
         this.delcolor.color("#00FFFF");
         this.affichage.color = "#00FFFF";
      }else if(e.cm > this.distance.la && e.cm < this.distance.si){
         this.amsynth.triggerAttackRelease('G4', '8n');
         this.affichage.text = "SI";
         this.delcolor.color("#04E762");
         this.affichage.color = "#04E762";
      }else if(e.cm > this.distance.si && e.cm < this.distance.max){
         this.amsynth.triggerAttackRelease('G5', '8n');
         this.affichage.text = "DO";
         this.delcolor.color("#00FF00");
         this.affichage.color = "#00FF00";
      }

      //Ce-ci mets à jour tout les paramètres
      this.amsynth.harmonicity.value = this.param.getValue("Harmonicity");
      this.amsynth.envelope.attack = this.param.getValue("Attack");
      this.amsynth.envelope.decay = this.param.getValue("Decay");
      this.amsynth.envelope.sustain = this.param.getValue("Sustain");
      this.amsynth.envelope.release = this.param.getValue("Release");
      this.amsynth.modulationEnvelope.attack = this.param.getValue("Modulator Attack");
      this.amsynth.modulationEnvelope.decay = this.param.getValue("Modulator Decay");
      this.amsynth.modulationEnvelope.sustain = this.param.getValue("Modulator Sustain");
      this.amsynth.modulationEnvelope.release = this.param.getValue("Modulator Release");

      this.affichage.x = this.canvas.width / 2 - this.affichage.getMeasuredWidth() / 2;
      this.affichage.updateCache();
    }else{
      //Sinon ont baisse le volume au minimum.
      if (this.amsynth.volume.value > this.volume.min) {
        this.amsynth.volume.cancelAndHoldAtTime();
        this.amsynth.volume.rampTo(this.volume.min, 0.1);
      }
    }
  }
}
